FROM node:16-alpine
WORKDIR /finanseapp_frontend
ENV PATH=".node_modules/.bin:$PATH"
COPY . .
RUN npm i react-scripts
RUN npm run build
CMD ["npm", "start"]