 import { useState } from 'react';
import './App.css'

function App() {

  const [version, setVersion] = useState();

  fetch(process.env.REACT_APP_PATH)
  .then(response => response.json())
  .then(data => setVersion(data.version));

  return (
   <>
    <h1 data-testid = "hello">Hello World</h1>
    <h2>Version: {version}</h2>
   </>
  );
}

export default App;
