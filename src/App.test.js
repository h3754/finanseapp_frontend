import { render, screen } from '@testing-library/react';
import App from './App';

test('shoukd have text content of "Hello World', () => {
  render(<App />);
  const helloH1 = screen.getByTestId("hello");
  expect(helloH1.innerHTML).toEqual("Hello World");
});
